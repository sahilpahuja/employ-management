<?php

namespace App\Http\Requests\Employee;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CreateEmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required',
            'address_1' => 'required',
            'city' => 'required',
            'zip_code' => 'nullable|integer|min:0|max:9999999999999999',
            'is_east' => ['integer', Rule::in(['0','1'])],
            'phone_nos' => 'array',
            'phone_nos.*' => 'required|string|min:8|max:15',
            'whatsapp_nos' => 'array',
            'whatsapp_nos.*' => 'required|string|min:8|max:15',
            'emails' => 'array',
            'emails.*' => 'required|email',
            'primary_phone_no' => ['required', 'string', 'min:8', 'max:15', Rule::in($this->phone_nos)],
            'primary_whatsapp_no' => ['required', 'string', 'min:8', 'max:15', Rule::in($this->whatsapp_nos)],
            'primary_email' => ['required', 'email', Rule::in($this->emails)],
        ];
    }
}
