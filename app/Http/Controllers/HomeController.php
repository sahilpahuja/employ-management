<?php

namespace App\Http\Controllers;

use App\Employee;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $employees = Employee::all();
//        $employees = Employee::all();
        return view('home', compact([
            'employees'
        ]));
    }

    public function show(Post $post)  {
        $categories = Category::all();
        $tags = Tag::all();
        return view('blog.post', compact([
            'post',
            'tags',
            'categories'
        ]));
    }
}
