<?php

namespace App\Http\Controllers;

use App\Address;
use App\Email;
use App\Employee;
use App\Http\Requests\Employee\CreateEmployeeRequest;
use App\Http\Requests\Employee\UpdateEmployeeRequest;
use App\Phone;
use App\Whatsapp;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EmployeeController extends Controller
{
    public function create()
    {
        return view('employees.create');
    }

    public function store(CreateEmployeeRequest $request)
    {
        $employee = Employee::create([
            'name'=>$request->name
        ]);

        Address::create([
            'employee_id' => $employee->id,
            'address_1'=> $request->address_1,
            'address_2'=> $request->address_2,
            'location' => $request->location,
            'zip_code'=> $request->zip_code,
            'postal_area'=> $request->postal_area,
            'is_east' => $request->is_east,
            'taluka' => $request->taluka,
            'suburb' => $request->suburb,
            'city' => $request->city,
            'district'=> $request->district,
            'state' => $request->state,
            'country' => $request->country,
        ]);

        foreach ($request->phone_nos as $phone){
            Phone::create([
                'employee_id' => $employee->id,
                'number' => $phone,
            ]);
        }

        foreach ($request->whatsapp_nos as $whatsapp_no){
            Whatsapp::create([
                'employee_id' => $employee->id,
                'number' => $whatsapp_no,
            ]);
        }

        foreach ($request->emails as $address){
            Email::create([
                'employee_id' => $employee->id,
                'address' => $address,
            ]);
        }

        $primary_phone_number = $employee->phone_nos()->where('number', $request->primary_phone_no)->first()->id;
        $primary_whatsapp_number = $employee->whatsapp_nos()->where('number', $request->primary_whatsapp_no)->first()->id;
        $primary_email_id = $employee->emails()->where('address', $request->primary_email)->first()->id;

        $employee->update([
            'primary_phone_id' => $primary_phone_number,
            'primary_whatsapp_id' => $primary_whatsapp_number,
            'primary_email_id' => $primary_email_id
        ]);

        session()->flash('success', 'Employee details Added Successfully');
        return redirect(route('home'));
    }

    public function show(Employee $employee)
    {
        return view('employees.show', compact('employee'));
    }

    public function edit(Employee $employee)
    {
        return view('employees.edit', compact('employee'));
    }

    public function update(UpdateEmployeeRequest $request, Employee $employee)
    {
        $employee->address->update([
            'address_1'=> $request->address_1,
            'address_2'=> $request->address_2,
            'location' => $request->location,
            'zip_code'=> $request->zip_code,
            'postal_area'=> $request->postal_area,
            'is_east' => $request->is_east,
            'taluka' => $request->taluka,
            'suburb' => $request->suburb,
            'city' => $request->city,
            'district'=> $request->district,
            'state' => $request->state,
            'country' => $request->country,
        ]);

        $employee->emails()->delete();
        $employee->phone_nos()->delete();
        $employee->whatsapp_nos()->delete();

        foreach ($request->phone_nos as $phone){
            Phone::create([
                'employee_id' => $employee->id,
                'number' => $phone,
            ]);
        }

        foreach ($request->whatsapp_nos as $whatsapp_no){
            Whatsapp::create([
                'employee_id' => $employee->id,
                'number' => $whatsapp_no,
            ]);
        }

        foreach ($request->emails as $address){
            Email::create([
                'employee_id' => $employee->id,
                'address' => $address,
            ]);
        }

        $primary_phone_number = $employee->phone_nos()->where('number', $request->primary_phone_no)->first()->id;
        $primary_whatsapp_number = $employee->whatsapp_nos()->where('number', $request->primary_whatsapp_no)->first()->id;
        $primary_email_id = $employee->emails()->where('address', $request->primary_email)->first()->id;

        $employee->update([
            'name'=>$request->name,
            'primary_phone_id' => $primary_phone_number,
            'primary_whatsapp_id' => $primary_whatsapp_number,
            'primary_email_id' => $primary_email_id
        ]);

        session()->flash('success', 'Employ details Updated Successfully!');
        return redirect(route('home'));

    }

    public function destroy(Employee $employee)
    {
        $employee->delete();

        session()->flash('success', 'Employ details Deleted Successfully!');
        return redirect(route('home'));
    }
}
