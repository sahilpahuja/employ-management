<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $fillable = [
        'employee_id', 'address_1', 'address_2', 'location', 'zip_code', 'postal_area', 'is_east', 'city', 'district', 'state', 'country', 'taluka', 'suburb'
    ];
}
