<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $fillable = ['employee_id', 'name', 'primary_phone_id', 'primary_whatsapp_id', 'primary_email_id'];

    public static function boot ()
    {
        parent::boot();

        Employee::deleting(function ($employee) {
            $employee->address()->delete();
            $employee->emails()->delete();
            $employee->phone_nos()->delete();
            $employee->whatsapp_nos()->delete();
        });
    }

    function address()
    {
        return $this->hasOne(Address::class);
    }

    function primaryPhone()
    {
        return $this->hasOne(Phone::class, 'id', 'primary_phone_id');
    }

    function primaryWhatsapp()
    {
        return $this->hasOne(Whatsapp::class, 'id', 'primary_whatsapp_id');
    }

    function primaryEmail()
    {
        return $this->hasOne(Email::class, 'id', 'primary_email_id');
    }

    function phone_nos()
    {
        return $this->hasMany(Phone::class);
    }

    function whatsapp_nos()
    {
        return $this->hasMany(Whatsapp::class);
    }

    function emails()
    {
        return $this->hasMany(Email::class);
    }
}
