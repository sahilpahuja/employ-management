@extends('layouts.app')

@section('page-level-styles')
    <style>
        @media (min-width: 900px) {
            .large_screen_padding {
                padding: 0 3rem;
            }
        }

        .scrollable{
            border: 1px solid #DDD;
            border-radius: 7px;
            overflow-y: scroll;
            max-height: 200px;
        }
        .scrollable::-webkit-scrollbar {
            width: 8px;               /* width of the entire scrollbar */
        }
        .scrollable::-webkit-scrollbar-track {
            background: #EEE;        /* color of the tracking area */
            border-radius: 10px;
            width: 1px;
        }
        .scrollable::-webkit-scrollbar-thumb {
            background-color: #CCC;    /* color of the scroll thumb */
            border-radius: 10px;       /* roundness of the scroll thumb */
        }
    </style>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
@endsection

@section('content')
<div class="container-fluid py-4 large_screen_padding">
    <div class="card">
        <div class="card-header h2 text-primary">Show Employee Details</div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-4">
                    <label>Name</label>
                    <input type="text" name="name" id="title" class="form-control" value="{{ $employee->name }}" readonly>
                </div>
            </div>
            <hr>
            <p class="h4 text-primary">Address Details</p>
            <div class="row">
                <div class="col-md-4 mb-2">
                    <label>Address 1 *</label>
                    <input type="text" name="address_1" id="address_1" class="form-control" value="{{ $employee->address->address_1 }}" readonly>
                </div>
                <div class="col-md-4 mb-2">
                    <label>Address 2</label>
                    <input type="text" name="address_2" id="address_2" class="form-control" value="{{ $employee->address->address_2 }}" readonly>
                </div>
                <div class="col-md-4 mb-2">
                    <label>Location</label>
                    <input type="text" name="location" id="location" class="form-control" value="{{ $employee->address->location }}" readonly>
                </div>
                <div class="col-md-4 mb-2">
                    <label>Zip / Postal Code</label>
                    <input type="text" name="zip_code" id="zip_code" class="form-control" value="{{ $employee->address->zip_code }}" readonly>
                </div>
                <div class="col-md-4 mb-2">
                    <label>Taluka</label>
                    <input type="text" name="taluka" id="taluka" class="form-control" value="{{ $employee->address->taluka }}" readonly>
                </div>
                <div class="col-md-4 mb-2">
                    <label>Suburb</label>
                    <input type="text" name="suburb" id="suburb" class="form-control" value="{{ $employee->address->suburb }}" readonly>
                </div>
                <div class="col-md-4 mb-2">
                    <label>East/West</label>
                    <input type="text" name="suburb" id="suburb" class="form-control" value="{{ ($employee->address->is_east)?"East":"West" }}" readonly>
                </div>
                <div class="col-md-4 mb-2">
                    <label>City *</label>
                    <input type="text" name="city" id="city" class="form-control" value="{{ $employee->address->city }}" readonly>
                </div>
                <div class="col-md-4 mb-2">
                    <label>District</label>
                    <input type="text" name="district" id="district" class="form-control" value="{{ $employee->address->district }}" readonly>

                </div>
                <div class="col-md-4 mb-2">
                    <label>State</label>
                    <input type="text" name="state" id="state" class="form-control" value="{{ $employee->address->state }}" readonly>

                </div>
                <div class="col-md-4 mb-2">
                    <label>Country</label>
                    <input type="text" name="country" id="country" class="form-control" value="{{ $employee->address->country }}" readonly>

                </div>
            </div>
            <hr>
            <p class="h4 text-primary">Contact Details</p>
            <div class="row">
                <div class="col-md-4 mb-2">
                    <div>
                        <label>Mobile Number</label>
                    </div>

                    <div id="phone_nos-container" class="px-2 scrollable">
                        @foreach($employee->phone_nos as $phone)
                            <div class="form-group">
                                <div class="d-flex justify-content-end align-items-center">
                                    <input type="radio" name="phone_nos_primary[]" {{ ($phone->id == $employee->primary_phone_id)?"checked":"" }}  class="mr-2 mb-2" disabled>
                                    <label for="phone_nos_primary[]">Primary</label>
                                </div>
                                <div class="d-flex">
                                    <input type="text" name="phone_nos[]" id="phone_nos" class="form-control" value="{{ $phone->number }}" readonly>
                                    <a href="tel:{{ $phone->number }}" class="btn btn-sm rounded-circle btn-primary ml-2"><i class="fa fa-2x fa-phone" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>

                <div class="col-md-4 mb-2">
                    <div>
                        <label>Whatsapp Number</label>
                    </div>

                    <div id="whatsapp_nos-container" class="px-2 scrollable">
                        @foreach($employee->whatsapp_nos as $number)
                            <div class="form-group">
                                <div class="d-flex justify-content-end align-items-center">
                                    <input type="radio" name="whatsapp_nos_primary[]" {{ ($number->id == $employee->primary_whatsapp_id)?"checked":"" }}  class="mr-2 mb-2" disabled>
                                    <label for="whatsapp_nos_primary[]">Primary</label>
                                </div>
                                <div class="d-flex">
                                    <input type="text" name="whatsapp_nos[]" class="form-control" value="{{ $number->number }}" readonly>
                                    <a href="https://wa.me/{{ $number->number }}" class="btn btn-sm rounded-circle btn-success ml-2"><i class="fa fa-2x fa-whatsapp" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        @endforeach
                    </div>

                </div>

                <div class="col-md-4 mb-2">
                    <div>
                        <label>Email</label>
                    </div>

                    <div id="emails-container" class="px-2 scrollable">
                        @foreach($employee->emails as $email)
                            <div class="form-group">
                                <div class="d-flex justify-content-end align-items-center">
                                    <input type="radio" name="emails_primary[]" {{ ($email->id == $employee->primary_email_id)?"checked":"" }} class="mr-2 mb-2" disabled>
                                    <label for="emails_primary[]">Primary</label>
                                </div>
                                <div class="d-flex">
                                    <input type="text" name="emails[]" class="form-control" value="{{ $email->address }}" readonly>
                                    <a href="mailto: {{ $email->address}}" class="btn btn-sm rounded-circle btn-danger ml-2"><i class="fa fa-2x fa-envelope-o" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="float-right">
                <a href="{{ route('home') }}" class="btn btn-secondary mt-4 mr-3">Home</a>
                <a href="{{ route('employees.edit', $employee->id) }}" class="btn btn-warning mt-4">Edit</a>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
@endsection
