@extends('layouts.app')

@section('page-level-styles')
    <style>
        @media (min-width: 900px) {
            .large_screen_padding {
                padding: 0 3rem;
            }
        }

        .scrollable{
            border: 1px solid #DDD;
            border-radius: 7px;
            overflow-y: scroll;
            max-height: 200px;
        }
        .scrollable::-webkit-scrollbar {
            width: 8px;               /* width of the entire scrollbar */
        }
        .scrollable::-webkit-scrollbar-track {
            background: #EEE;        /* color of the tracking area */
            border-radius: 10px;
            width: 1px;
        }
        .scrollable::-webkit-scrollbar-thumb {
            background-color: #CCC;    /* color of the scroll thumb */
            border-radius: 10px;       /* roundness of the scroll thumb */
        }
    </style>

    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
@endsection

@section('content')
<div class="container-fluid py-4 large_screen_padding">
    <div class="card">
        <div class="card-header h2 text-primary">Edit Employee</div>
        <div class="card-body">
            <form id="employee_form" action="{{route('employees.update', $employee->address->id)}}" method="POST">
                @csrf
                @method('PUT')
                <div class="row">
                    <div class="col-md-4 mb-2">
                        <label>Name *</label>
                        <input type="text" name="name" id="title" class="form-control" value="{{ old('name', $employee->name) }}">
                        @error('name')
                            <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <hr>
                <p class="h4 text-primary">Address Details</p>
                <div class="row">
                    <div class="col-md-4 mb-2">
                        <label>Address 1 *</label>
                        <input type="text" name="address_1" id="address_1" class="form-control" value="{{ old('address_1', $employee->address->address_1) }}">
                        @error('address_1')
                            <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="col-md-4 mb-2">
                        <label>Address 2</label>
                        <input type="text" name="address_2" id="address_2" class="form-control" value="{{ old('address_2', $employee->address->address_2) }}">
                        @error('address_2')
                            <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="col-md-4 mb-2">
                        <label>Location</label>
                        <input type="text" name="location" id="location" class="form-control" value="{{ old('location', $employee->address->location) }}">
                        @error('location')
                            <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="col-md-4 mb-2">
                        <label>Zip / Postal Code</label>
                        <input type="text" name="zip_code" id="zip_code" class="form-control" value="{{ old('zip_code', $employee->address->zip_code) }}">
                        @error('zip_code')
                            <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="col-md-4 mb-2">
                        <label>Taluka</label>
                        <input type="text" name="taluka" id="taluka" class="form-control" value="{{ old('taluka', $employee->address->taluka) }}">
                        @error('taluka')
                            <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="col-md-4 mb-2">
                        <label>Suburb</label>
                        <input type="text" name="suburb" id="suburb" class="form-control" value="{{ old('suburb', $employee->address->suburb) }}">
                        @error('suburb')
                            <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="col-md-4 mb-2">
                        <label>East / West</label>
                        <div class="form-group">
                            <select class="d-block form-control" name="is_east">
                                <option value="1" {{ old('is_east', $employee->address->is_east)==1?"selected":"" }}>East</option>
                                <option value="0" {{ old('is_east', $employee->address->is_east)==0?"selected":"" }}>West</option>
                            </select>
                        </div>
                        @error('is_east')
                            <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="col-md-4 mb-2">
                        <label>City *</label>
                        <input type="text" name="city" id="city" class="form-control" value="{{ old('city', $employee->address->city) }}">
                        @error('city')
                            <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="col-md-4 mb-2">
                        <label>District</label>
                        <input type="text" name="district" id="district" class="form-control" value="{{ old('district', $employee->address->district) }}">
                        @error('district')
                            <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="col-md-4 mb-2">
                        <label>State</label>
                        <input type="text" name="state" id="state" class="form-control" value="{{ old('state', $employee->address->state) }}">
                        @error('state')
                            <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="col-md-4 mb-2">
                        <label>Country</label>
                        <input type="text" name="country" id="country" class="form-control" value="{{ old('country', $employee->address->country) }}">
                        @error('country')
                            <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <hr>
                <p class="h4 text-primary">Contact Details</p>
                <div class="row">
                    <div class="col-md-4 mb-2">
                        <div>
                            <label>Mobile Number</label>
                            <a type="button" onclick="addMoreFields(this)" data-name="phone_nos" class="btn-sm px-2 py-1 btn-primary"><i class="fa fa-plus"></i></a>
                        </div>
                        @error('primary_phone_no')
                            <div class="text-danger">{{ $message }}</div>
                        @enderror
                        @error('phone_nos')
                            <div class="text-danger">{{ $message }}</div>
                        @enderror
                        @if($errors->has('phone_nos.*'))
                            <div class="text-danger">There are errors in one or more phone numbers</div>
                            <ul>
                                @foreach($errors->get('phone_nos.*') as $errs)
                                    @foreach($errs as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                @endforeach
                            </ul>
                        @endif
                        <div id="phone_nos-container" class="scrollable px-4">
                            @foreach($employee->phone_nos as $phone)
                                <div class="form-group">
                                    <div class="d-flex justify-content-between">
                                        @if($loop->index > 0)
                                            <a type="button" onclick="removeField(this)" data-name="phone_nos" class="btn-sm px-2 py-1 btn-danger mb-2"><i class="fa fa-trash"></i></a>
                                        @else
                                            <div></div>
                                        @endif
                                        <div class="d-flex justify-content-end align-items-center">
                                            <input type="radio" data-id="{{ $loop->index }}" name="phone_nos_primary[]" {{ ($phone->id == $employee->primary_phone_id)?"checked":"" }} class="mr-2 mb-2">
                                            <label for="phone_nos_primary[]">Primary</label>
                                        </div>
                                    </div>
                                    <input type="text" data-id="{{ $loop->index }}" name="phone_nos[]" id="phone_nos" class="form-control" value="{{ $phone->number }}">
                                </div>
                            @endforeach
                        </div>
                    </div>

                    <div class="col-md-4 mb-2">
                        <div>
                            <label>Whatsapp Number</label>
                            <a type="button" onclick="addMoreFields(this)" data-name="whatsapp_nos" class="btn-sm px-2 py-1 btn-primary"><i class="fa fa-plus"></i></a>
                        </div>
                        @error('primary_whatsapp_no')
                            <div class="text-danger">{{ $message }}</div>
                        @enderror
                        @error('whatsapp_nos')
                            <div class="text-danger">{{ $message }}</div>
                        @enderror
                        @if($errors->has('whatsapp_nos.*'))
                            <div class="text-danger">There are errors in one or more whatsapp numbers</div>
                            <ul>
                                @foreach($errors->get('whatsapp_nos.*') as $errs)
                                    @foreach($errs as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                @endforeach
                            </ul>
                        @endif
                        <div id="whatsapp_nos-container" class="scrollable px-4">
                            @foreach($employee->whatsapp_nos as $number)
                                <div class="form-group">
                                    <div class="d-flex justify-content-between">
                                        @if($loop->index > 0)
                                            <a type="button" onclick="removeField(this)" data-name="whatsapp_nos" class="btn-sm px-2 py-1 btn-danger mb-2"><i class="fa fa-trash"></i></a>
                                        @else
                                            <div></div>
                                        @endif
                                        <div class="d-flex justify-content-end align-items-center">
                                            <input type="radio" data-id="{{ $loop->index }}" name="whatsapp_nos_primary[]" {{ ($number->id == $employee->primary_whatsapp_id)?"checked":"" }} class="mr-2 mb-2">
                                            <label for="whatsapp_nos_primary[]">Primary</label>
                                        </div>
                                    </div>
                                    <input type="text" data-id="{{ $loop->index }}" name="whatsapp_nos[]" id="whatsapp_nos" class="form-control"  value="{{ $number->number }}">
                                </div>
                            @endforeach
                        </div>
                    </div>

                    <div class="col-md-4 mb-2">
                        <div>
                            <label>Email</label>
                            <a type="button" onclick="addMoreFields(this)" data-name="emails" class="btn-sm px-2 py-1 btn-primary"><i class="fa fa-plus"></i></a>
                        </div>
                        @error('primary_email')
                            <div class="text-danger">{{ $message }}</div>
                        @enderror
                        @error('emails')
                            <div class="text-danger">{{ $message }}</div>
                        @enderror
                        @if($errors->has('emails.*'))
                            <div class="text-danger">There are errors in one or more emails numbers</div>
                            <ul>
                                @foreach($errors->get('emails.*') as $errs)
                                    @foreach($errs as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                @endforeach
                            </ul>
                        @endif

                        <div id="emails-container" class="scrollable px-4">
                            @foreach($employee->emails as $email)
                                <div class="form-group">
                                    <div class="d-flex justify-content-between">
                                        @if($loop->index > 0)
                                            <a type="button" onclick="removeField(this)" data-name="emails" class="btn-sm px-2 py-1 btn-danger mb-2"><i class="fa fa-trash"></i></a>
                                        @else
                                            <div></div>
                                        @endif
                                        <div class="d-flex justify-content-end align-items-center">
                                            <input type="radio" data-id="{{ $loop->index }}" name="emails_primary[]" {{ ($email->id == $employee->primary_email_id)?"checked":"" }} class="mr-2 mb-2">
                                            <label for="emails_primary[]">Primary</label>
                                        </div>
                                    </div>
                                    <input type="text" data-id="{{ $loop->index }}" name="emails[]" id="emails" class="form-control"  value="{{ $email->address }}">
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="float-right">
                    <a href="{{ route('home') }}" class="btn btn-secondary mt-4 mr-3">Cancel</a>
                    <button type="submit" class="btn btn-warning mt-4">Update</button>
                </div>
                <div class="clearfix"></div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('page-level-scripts')
    <script>
        let data_counter = {'emails_primary_id':{{ $employee->emails->count() }}, 'phone_nos_primary_id':{{ $employee->phone_nos->count() }}, 'whatsapp_nos_primary_id':{{ $employee->whatsapp_nos->count() }} };
        function addMoreFields(button) {
            let name = $(button).data('name');
            let container = $("#"+name+"-container");
            console.log($(`input[name='${name}_primary[]']`));
            container.append(`
                <div class="form-group">
                    <div class="d-flex justify-content-between">
                        <a type="button" onclick="removeField(this)" data-name="emails" class="btn-sm px-2 py-1 btn-danger mb-2"><i class="fa fa-trash"></i></a>
                        <div class="d-flex justify-content-end align-items-center">
                            <input type="radio" name="${name}_primary[]" data-id="${data_counter[name+"_primary_id"]}" class="mr-2 mb-2">
                            <label for="${name}_primary[]">Primary</label>
                        </div>
                    </div>
                    <input type="text" name="${name}[]" data-id="${data_counter[name+"_primary_id"]}" class="form-control">
                </div>
            `)
            data_counter[name+"_primary_id"] += 1;
        }

        function removeField(button) {
            console.log($(button).parent().parent().remove())
        }

        function getPrimaryDetail(name){
            let primary_index = $(`input[name='${name}_primary[]']:checked`).data('id');
            return  $(`input[name='${name}[]'][data-id=${primary_index}]`).val();
        }

        $('#employee_form').submit(function () {
            $(this).append(`<input type="hidden" name="primary_phone_no" value="${getPrimaryDetail('phone_nos')}" />`);
            $(this).append(`<input type="hidden" name="primary_whatsapp_no" value="${getPrimaryDetail('whatsapp_nos')}" />`);
            $(this).append(`<input type="hidden" name="primary_email" value="${getPrimaryDetail('emails')}" />`);
            return true;
        });
    </script>
@endsection
