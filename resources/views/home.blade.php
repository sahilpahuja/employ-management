@extends('layouts.app')

@section('page-level-styles')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css">
    <style>
        @media (max-width: 800px) {
            .remove_small {
                display: none;
            }
        }

        .scrollable{
            overflow-y: scroll;
            max-height: 600px;
        }
        .scrollable::-webkit-scrollbar {
            width: 8px;               /* width of the entire scrollbar */
        }
        .scrollable::-webkit-scrollbar-track {
            background: #EEE;        /* color of the tracking area */
            border-radius: 10px;
            width: 1px;
        }
        .scrollable::-webkit-scrollbar-thumb {
            background-color: #CCC;    /* color of the scroll thumb */
            border-radius: 10px;       /* roundness of the scroll thumb */
        }
    </style>
@endsection

@section('content')
    <div class="container-lg">
        <div class="row justify-content-center">
            <a href="{{ route('employees.create') }}" class="btn btn-primary mb-3 d-block">Add Employee</a>

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Manage Employees</div>
                    <div class="card-body scrollable">
                        @if($employees->count() > 0)
                            <table class="table table-bordered" id="employees-table">
                                <thead>
                                <th>Name</th>
                                <th class="remove_small">Phone No.</th>
                                <th class="remove_small">Whatsapp No.</th>
                                <th class="remove_small">Email</th>
                                <th>Actions</th>
                                </thead>
                                <tbody>
                                @foreach($employees as $employee)
                                    <tr>
                                        <td>{{ $employee->name }}</td>
                                        <td class="remove_small"><a href="tel:{{ $employee->primaryPhone->number }}">{{ $employee->primaryPhone->number }}</a></td>
                                        <td class="remove_small"><a href="https://wa.me/{{ $employee->primaryWhatsapp->number }}" target=”_blank”>{{ $employee->primaryWhatsapp->number }}</a></td>
                                        <td class="remove_small"><a href="mailto: {{ $employee->primaryEmail->address}}" target=”_blank”>{{ $employee->primaryEmail->address }}</a></td>
                                        <td>
                                            <a href="{{ route('employees.show', $employee->id) }}" class="btn btn-sm btn-outline-primary">View</a>
                                            <a href="{{ route('employees.edit', $employee->id) }}" class="btn btn-sm btn-outline-warning">Edit</a>
                                            <a class="btn btn-sm btn-outline-danger" onclick="displayModalForm({{$employee->id}})" data-toggle="modal" data-target="#deleteModal">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <h4>No Employees Available</h4>
                        @endif
                    </div>
{{--                    <div class="card-footer">--}}
{{--                        {{ $employees->links() }}--}}
{{--                    </div>--}}
                </div>
            </div>
        </div>
    </div>

    <!-- DELETE MODAL -->
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog"aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Delete Modal</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="" METHOD="POST" id="deleteForm">
                    @csrf
                    @method('DELETE')
                    <div class="modal-body">
                        <p>Are you sure, you want to delete this employee's info?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times mr-2"></i>Close</button>
                        <button type="submit" class="btn btn-danger"><i class="fa fa-trash mr-2"></i>Delete</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('page-level-scripts')
    <script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function ($) {
            $('#employees-table').DataTable({
                "lengthMenu": [ 5, 10, 20, 75, 100],
                "columnDefs": [{
                    'orderable': false,
                    "targets": [-1],
                }, { "bSearchable": true, "aTargets": [-1] }
                ],
                "pagingType": "full_numbers",
                "paging": true,
            });
        });

        function displayModalForm($id) {
            var url = 'employees/' + $id;
            $("#deleteForm").attr('action', url);
        }
    </script>
@endsection
